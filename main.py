#!/usr/bin/env python3
#
# Copyright Graham Lee 2022.
#
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

import argparse
import csv
import os

from codeoutliner import OutlineRoot

def convert_codebook(infile: str, outfile: str) -> None:
    """
    Read a TSV file representing an exported NVivo codebook.
    Write an OPML file representing an OmniOutliner outline document.
    """
    with open(infile, newline='') as in_tsv:
        reader = csv.DictReader(in_tsv, delimiter='\t')
        top = OutlineRoot(os.path.basename(infile))
        for row in reader:
            elements = row['Name'].split('\\')
            elements.insert(0, row['Folder'])
            leaf = top
            for element in elements:
                leaf.add(element)
                leaf = leaf.child_with_text(element)
    with open(outfile, 'w') as out_opml:
        out_opml.write(top.xml_representation())
                

def main():
    """
    Read arguments from the command line and invoke convert_codebook.
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("infile", help="The NVivo codebook (in TSV format) you want to convert.")
    parser.add_argument("outfile", help="The outline (in OPML format) you want to create.")
    args = parser.parse_args()
    convert_codebook(args.infile, args.outfile)


if __name__ == '__main__':
    main()
