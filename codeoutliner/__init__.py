#
# Copyright Graham Lee 2022.
#
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

from datetime import datetime
from email import utils
from ordered_set import OrderedSet
from typing import Union

__version__ = '1.0.0'

class Outline:
    """Represents an element in a structured document.
    Elements must have some text, and optionally
    child elements (which are also Outlines): this
    is the Composite pattern."""
    def __init__(self, text: str):
        self.text = text
        self.children = OrderedSet()
    
    def add(self, text: str) -> None:
        """Add a child element to my children.
        The child is provided as text and converted
        into an empty Outline then added.
        If I already have a child with
        the same text as the supplied child, I
        do not change."""
        existing_child = self.child_with_text(text)
        if existing_child is None:
            self.children.add(Outline(text))
    
    def text_equal(self, text: str) -> bool:
        """Test whether my text is the same as that
        supplied. This is a custom method not an
        override for __eq__ because true equality
        requires that my children be the same as
        another Outline's too."""
        return self.text == text
    
    def child_with_text(self, text: str) -> Union['Outline', None]:
        """Return my child with the supplied text. If
        I have no such child, return None."""
        matching = [c for c in self.children if c.text_equal(text)]
        if len(matching) == 0:
            return None
        return matching[0]
    
    def __str__(self) -> str:
        return "Outline: " + self.internal_formatted_outline(0)
    
    def internal_formatted_outline(self, indent) -> str:
        """Used to nicely nest my string representation."""
        leader = '\t'*indent
        text = f'{leader}{self.text}\n'
        for child in self.children:
            text += child.internal_formatted_outline(indent+1)
        return text

    def xml_representation(self,indent=0) -> str:
        """
        Represent myself as an XML element.
        """
        leader = '\t' * indent
        if len(self.children) > 0:
            rep = f'{leader}<outline text="{self.xml_safe_text()}">\n'
            for child in self.children:
                rep += child.xml_representation(indent+1)
            rep += f'{leader}</outline>\n'
            return rep
        else:
            return f'{leader}<outline text="{self.xml_safe_text()}"/>\n'
    
    def xml_safe_text(self) -> str:
        """
        Do some very limited manipulation of my text to try to ensure
        that it is valid in XML.
        """
        return self.text.replace('&', '&amp;')


class OutlineRoot(Outline):
    """A special variety of Outline for use at the top level.
    It has a different XML representation."""
    def __init__(self, title):
        super().__init__(title)
    
    def __str__(self) -> str:
        return "OutlineRoot: " + self.internal_formatted_outline(0)

    def xml_representation(self, indent=0) -> str:
        """
        Represent myself as an XML document.
        """
        rep = """<?xml version="1.0" encoding="UTF-8"?>
<opml version="2.0">
<head>"""
        rep += f'<title>{self.xml_safe_text()}</title>\n'
        rep += f'<dateCreated>{utils.format_datetime(datetime.now())}</dateCreated>\n'
        rep += '</head>\n<body>\n'
        for child in self.children:
            rep += child.xml_representation(1)
        rep += '</body></opml>\n'
        return rep

