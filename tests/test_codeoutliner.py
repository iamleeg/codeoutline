#
# Copyright Graham Lee 2022.
#
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

import pytest

from codeoutliner import Outline, OutlineRoot


def test_outline_cannot_have_two_children_with_the_same_text():
    outline = Outline('parent')
    outline.add('child')
    outline.add('child')
    assert(len(outline.children) == 1)

def test_child_of_outline_is_not_replaced_on_second_add():
    parent = Outline('parent')
    parent.add('child')
    child = parent.child_with_text('child')
    parent.add('child')
    assert(parent.child_with_text('child') == child)

def test_child_of_outline_is_none_if_requested_child_was_never_added():
    parent = Outline('parent')
    parent.add('child')
    child = parent.child_with_text('something')
    assert(child is None)

def test_text_equal_returns_true_if_text_matches_argument():
    outline = Outline('text')
    assert(outline.text_equal('text'))

def test_text_equal_returns_false_if_text_does_not_match_argument():
    outline = Outline('text')
    assert(not outline.text_equal('other text'))

def test_xml_representation_of_text_uses_entity_for_ampersand():
    outline = Outline('Fish & chips')
    assert(outline.xml_safe_text() == 'Fish &amp; chips')

def test_xml_representation_of_outline_with_no_children():
    outline = Outline('Buy bread')
    assert(outline.xml_representation() == '<outline text="Buy bread"/>\n')

def test_xml_representation_of_outline_with_children():
    outline = Outline('Buy bread')
    outline.add('wholemeal')
    outline.add('white')
    xml_document = '<outline text="Buy bread">\n\t<outline text="wholemeal"/>\n\t<outline text="white"/>\n</outline>\n'
    assert(outline.xml_representation() == xml_document)

@pytest.mark.freeze_time('2022-05-12')
def test_xml_representation_of_outline_root():
    root = OutlineRoot('Title')
    root.add('item')
    xml_document = """<?xml version="1.0" encoding="UTF-8"?>
<opml version="2.0">
<head><title>Title</title>
<dateCreated>Thu, 12 May 2022 00:00:00 -0000</dateCreated>
</head>
<body>
	<outline text="item"/>
</body></opml>
"""
    assert(root.xml_representation() == xml_document)
