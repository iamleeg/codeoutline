============
CodeOutliner
============

What it Does
============

Turn a codebook exported from `NVivo <https://www.qsrinternational.com/nvivo-qualitative-data-analysis-software/home>`_ (in what it calls CSV format, which is actually tab-separated) into an OPML document which can then be viewed and manipulated in any outliner tool (I use `OmniOutliner <https://www.omnigroup.com/omnioutliner>`_).

How to Use It
=============

You need `poetry <https://python-poetry.org>`_, then you can run it::

  $ poetry install
  [...]
  $ poetry run ./main.py --help
  usage: main.py [-h] infile outfile

  positional arguments:
    infile      The NVivo codebook (in TSV format) you want to convert.
    outfile     The outline (in OPML format) you want to create.

Run the Tests
=============

Again, you can use poetry::

  $ poetry run pytest

To Do
=====

It'd be nice to have better XML-safety: probably replacing all of the string manipulation with the Python XML library.

Licence
=======

See ``COPYING``.
